FROM php:5.6-apache

ENV OJS_VERSION 3.1.1-4
RUN usermod --uid 1000 www-data && \
    groupmod --gid 1000 www-data && \
    apt-get update && \
    apt-get install -y libpq-dev && \
    docker-php-ext-install pgsql && \
    curl -L https://pkp.sfu.ca/ojs/download/ojs-${OJS_VERSION}.tar.gz | tar xz --strip-components=1
